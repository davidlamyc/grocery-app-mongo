//Load libraries
const path = require("path");
const q = require("q");
const express = require("express");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const Sequelize = require ('sequelize');
const db = require("./database");
var mongoose = require('mongoose');
//Create an instance of express
const app = express();

var Groceries = "";
//connection perimeter is a callback to the mongo connection.
db.getDB(function(connection){
	//connect to specifically groceries collection
	Groceries = connection.collection('groceries');
});

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));


app.get("/api/products", function(req, resp) {
	console.log("search Groceries");
    var sortBy = parseInt(req.query.sortBy) || 1; 
    var name = req.query.searchName;
    var brand = req.query.brand;
	console.log("Search grocercies DB by ~")
	console.log("name:", name);
	console.log("brand:", brand);
	console.log("sortBy:", sortBy);

	if (name && brand){
		Groceries.find({brand:brand, name: {$regex:/name/}}
		).sort({name: sortBy})
		.limit(20)
		.toArray(function(err,groceries){
			//console.log(groceries);
			console.log("----",groceries)
			resp.json(groceries);
		})
	}
	else if (brand) {
		Groceries
			.find({brand:brand})
			.sort({name: sortBy})
			.limit(20)
			.toArray(function(err,groceries)
			{
			//console.log(groceries);
			resp.json(groceries);
		})
	} else if (name) {
		Groceries.find({name:{$regex:/name/}})
			.sort({name: sortBy})
			.limit(20)
			.toArray(function(err,groceries){
			//console.log(groceries);
			resp.json(groceries);
		}
	)} else {
		Groceries.find({}).limit(20).sort({name: sortBy})
		.toArray(function(err,groceries){
			//console.log(groceries);
			resp.json(groceries);
		})
	}
});

//OBTAIN SUM OF GROCERIES
app.get("/api/products/sum", function(req,res){
	Groceries.count({}, function(err,result){
		res.json(result);
	});
});

//GET PRODUCT BY productID
app.get("/api/products/:productId", function(req, resp) {
	var id = mongoose.Types.ObjectId(req.params.productId);
	console.log("Query string is", id);
	Groceries.findOne({ _id: id}, function(err,result){;
		console.log("Product to edit", result);
		resp.json(result);
	})
});

//OBTAIN CATEGORIES
app.get("/api/brands", function(req,res){
	console.log("Getting brands");
	Groceries.distinct("brand",
		function(err,result){
		console.error("Err is:", err)
		res.json(result);
		}
	)}
);

//ADD PRODUCT
app.post("/api/products/", function(req, resp) {
	console.log(" add Grocery ");
	console.log(req.body.upc12);
	console.log(req.body.brand);
	console.log(req.body.name);
	Groceries.update({upc12:req.body.upc12},{upc12:req.body.upc12,brand:req.body.brand,name:req.body.name },{
            upsert: true
    }, function(err, r) {
        console.log("Product Added:", req.body.name)
	});
});

//EDIT PRODUCTS
app.put("/api/products/:productId", function(req, resp) {
	console.log(" update Grocery  ");
	Groceries.findOneAndUpdate({upc12:req.body.upc12}, {$set: {upc12:req.body.upc12,brand:req.body.brand,name:req.body.name}}, 
	{
    upsert: true
    }, function(err, r) {
      console.log(err);
      console.log(r);
    });
});

//DELETE USER
app.delete("/api/products/:productId", function(req, resp) {
	console.log(" delete Grocery ");
	Groceries.destroy(
		{where: {id:req.params.id}
		}).then(function(result){
    console.log(result);
	})
});

//Static routes
app.use(express.static(path.join(__dirname, "../client")));

//Configure the server
const port = process.env.APP_PORT || 3000;
app.listen(port, function() {
	console.info("Application started on port %d", port);
});

