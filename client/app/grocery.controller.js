(function () {
    angular.module("MyGroceryApp")
        .controller("ListCtrl", ListCtrl)
        .controller("AddCtrl", AddCtrl)
        .controller("EditCtrl", EditCtrl);

    ListCtrl.$inject = ['GroceryService', '$state', '$http'];

    function ListCtrl(GroceryService, $state, $http) {
        var vm = this;
        vm.products = "";
        vm.brands = "";//array to display on view
        vm.brand = "";// individual query
        vm.sortBy = "";
        vm.upc12 = "";
        vm.searchName = "";

        vm.totalItems = 0;
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.maxSize = 5; // control this number of display items on the pagination.

        //receives product + attributes from "list" state, passes to "edit" state
        vm.getProduct = function (product) {
            console.log("product is", product)
            $state.go("edit", {'productId': product._id});
        };


        vm.pageChanged = function() {
            console.log('Page changed to: ' + vm.currentPage);
            GroceryService.search(vm.searchType, vm.keyword, vm.sortBy, vm.itemsPerPage, vm.currentPage)
                .then(function (products) {
                    vm.products = products;
                    GroceryService.sum().then(function(result){
                        console.log(result);
                        vm.totalItems = result.sum;
                    });
                }).catch(function (err) {
                console.info("Some Error Occurred",err)
            });
        };

        vm.search = function (brand, searchName, sortBy) {
            console.log("brand:", brand);
            console.log("searchName:", searchName);
            console.log("sortBy:", sortBy);
                GroceryService.search(brand, searchName, sortBy)
                    .then(function (products) {
                        console.log(products);
                        vm.products = products;
                        GroceryService.sum().then(function(result){
                            console.log(result);
                            vm.totalItems = result.sum;
                        });
                    })
                    .catch(function (err) {
                        console.info("Some Error Occurred",err);
                    });
            
        };

        vm.initBrands = function(){
            console.log("Getting brands");
            $http.get("/api/brands")
                .then(function(result){
                    //use categories variable
                    vm.brands = result.data;
                })
                .catch(function(err){
                    console.log(err);
                })
        }
        vm.initBrands();
        

        GroceryService.search(vm.brand, vm.searchName, vm.sortBy)
            .then(function(products) {
                console.log("refresh list");
                vm.products = products;
            }).catch(function (err) {
            console.info("Some Error Occurred",err)
        });

    }

    EditCtrl.$inject = ['GroceryService', '$stateParams', '$state'];

    function EditCtrl(GroceryService, $stateParams, $state) {
        var vm = this;
        vm.product = {};
        
        vm.cancel = function () {
            $state.go("list");
        };
        GroceryService.edit($stateParams.productId)
            .then(function (product) {
                console.log(product);
                vm.product = product;
            }).catch(function (err) {
                console.info("Some Error Occurred",err)
            });

        vm.save = function() {
            console.log("Saving the changes");
            GroceryService.save(vm.product)
                .then(function (result) {
                    console.info("Product saved. Product is:", result);
                    $state.go("list");
                }).catch(function (err) {
                console.info("Some Error Occurred", err)
            });
        }

        vm.remove = function () {
            console.log("Removing this product");
            if (confirm("Do you really want to remove this product from your Groceries?") == true) {
                GroceryService.remove(vm.product)
                    .then(function (result) {
                        console.info("Product removed.");
                        $state.go("list");
                    }).catch(function (err) {
                    console.info("Some Error Occurred",err)
                });
            } else {
                //do nothing
            }
        }
    }

    AddCtrl.$inject = ['GroceryService', '$stateParams', '$state'];
    
    function AddCtrl(GroceryService, $stateParams, $state) {
        var vm = this;
        vm.product = {};
        vm.UPC12Exist = false;
        vm.cancel = function () {
            $state.go("list");
        };
        
        vm.save = function () {
            console.log("Saving the changes");
            GroceryService.add(vm.product)
                .then(function (result) {
                    console.info("Product saved.");
                    $state.go("list");
                }).catch(function (err) {
                    console.info("Some Error Occurred",err);
                });

            }
    };
})();